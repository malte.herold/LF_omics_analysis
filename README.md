# General

Rscripts in the respective `src/`directories can be run after adjusting the 
`REPODIR` variable (path to the repository), and for some `RESULTSDIR` (path to the output dir of the RNAseq analysis pipeline)
Update: For RNAseq the R-script for analysis were included in the snakemake workflow or should be called with REPODIR and RESULTSDIR as arguments

## R libraries required
* library(tidyverse)
* library(plyr)
* library(readxl)
* library(stringr)
* library(ggfortify)
* library(reshape2)
* library(dplyr)
* library(tidyr)
* library(preprocessCore)
* library(DESeq2)



# Annotation Tables

Auxillary files, some from another repository: https://git-r3lab.uni.lu/malte.herold/LF_annotation_new


# Proteomics Workflow

Maxquant was used for quantification and Perseus for generating the input files for the analysis steps performed here.

Conversion of identifiers, reading in of LFQ intensities and scaling with the script [LF_lfqs_analysis.R](Proteomics/src/LF_lfqs_analysis.R)

Functional annotations are added to output of t-test performed in Perseus with [t_test_funcats.R](Proteomics/src/t_test_funcats.R)



# RNAseq workflow

## Dependencies

### General

Linux system

Illumina paired-end reads


### snakemake

Install snakemake https://bitbucket.org/snakemake/snakemake/wiki/Home, e.g.:

`sudo apt install snakemake`

### Trimmomatic
http://www.usadellab.org/cms/?page=trimmomatic

e.g.

`conda install trimmomatic`

### bowtie2
http://bioconda.github.io/recipes/bowtie2/README.html

e.g.

`conda install bowtie2`


### samtools
`wget https://github.com/samtools/samtools/releases/download/1.5/samtools-1.5.tar.bz2`

`tar xvjf samtools-1.5.tar.bz2`

`cd samtools-1.5`

`./configure --prefix=/where/to/install`

`make`

`make install`

add to ~/.bashrc:
`export PATH=$PATH:/installdir/bin`


### FastQC
http://www.bioinformatics.babraham.ac.uk/projects/fastqc/

e.g.

`conda install fastqc`

### FeatureCounts

http://subread.sourceforge.net/

`conda install subread`


### Conversion to TPM
A combined table of transcripts per million (TPM) normalized values is generated with [normalize_featurecounts.R](RNAseq/src/normalize_featurecounts.R)

### Deseq2 comparison 
Counted features are compared with deseq2 and functional annotation is appended to the final output in [deseq2_comparison.R](RNAseq/src/deseq2_comparison.R)

## input files and running the pipeline

### Input for snakemake variables

For an example launcher script see [run_pipeline.sh](RNAseq/run_pipeline.sh)

Files for samples, directories etc. are passed to snakemake as environment variables, which in this case are defined in the launcher script:
* `OUTPUTDIR`
* `SAMPLES`
* `REFERENCES`
* `GFFS`
* `REPODIR`

### set parameters

adjust paramters for trimming in [trimming.rules](RNAseq/rules/trimming.rules)

e.g. add path to adapter file, if installed with conda:
`miniconda3/pkgs/trimmomatic-0.36-3/share/trimmomatic/adapters`


### Raw reads

paired files in one directory per sample and renamed as R1.fastq.gz and R2.fastq.gz, e.g. 

`Rawreaddir/sample1/R1.fastq.gz`

`Rawreaddir/sample1/R2.fastq.gz`


### Running the workflow

Either with the launcher script or exporting the necessary variables and simply running by executing `snakemake` in the directory with the [Snakefile](RNAseq/Snakefile)



###


# Combined

Summary of both proteomics and transcriptomics and scripts for plotting.

A combined table of all values (raw readcounts, tpm, lfq intensities (scaled and unscaled)) for the two conditions is merged with functional annotations.

Plotting scripts for generating the basis for figure3 and figure4.


