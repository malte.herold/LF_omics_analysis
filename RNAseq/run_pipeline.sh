#!/bin/bash -l


#source src/preload_modules.sh

export OUTPUTDIR='/media/storage/Work_Storage/SysMetEx/LF_Analysis/RNAseq'
#export OUTPUTDIR="."
export INPUTDIR='/media/storage/Work_Storage/SysMetEx/rawdata/'
export SAMPLES=$(\ls -d /media/storage/Work_Storage/SysMetEx/rawdata/P* | parallel "echo {/}")
export SAMPLES="P3251_101"
export REFERENCES='/home/mh/Uni_Lux/LF_omics_analysis/RNAseq/data/lf_chrm.fna'
GFFDIR="/home/mh/Uni_Lux/LF_omics_analysis/RNAseq/data"
export GFFS="$GFFDIR/lf_chrm.gff"
REPODIR="/home/mh/Uni_Lux/LF_omics_analysis"

#snakemake -j4 -np
snakemake -p --rerun-incomplete

#snakemake --dag | dot -T png > dag.png
#snakemake --rulegraph | dot -T png > rulegraph.png
