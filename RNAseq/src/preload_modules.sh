
#source this file before executing pipeline
#

#module load lang/Python/3.4.3-ictce-7.3.5
source /mnt/nfs/projects/ecosystem_biology/local_tools/pyenv/load_dependencies.sh
pyenv global 3.4.3

module load bio/FastQC
module load bio/Bowtie2
module load bio/SAMtools

#featureCounts path
export PATH=/home/users/aheintzbuschart/bin/subread-1.5.1-Linux-x86_64/bin:$PATH


