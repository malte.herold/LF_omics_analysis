tar xfv axenic_base_rawdata.tar

#from /media/storage/Work_Storage/SysMetEx/rawdata

SAMPLES=P3251_103 P3251_104 P3251_105 P6161_101  P6161_102  P6161_103  P6161_104  P6161_105  P6161_106

cwd=$(pwd)
for sample in $(find axenic_base -name P3251_10[345])
do
	bs=$(basename $sample)
	mkdir $bs
	ln -s ${cwd}/${sample}/*/*_1.fastq.gz ${cwd}/${bs}/R1.fastq.gz
	ln -s ${cwd}//${sample}/*/*_2.fastq.gz ${cwd}/${bs}/R2.fastq.gz
done

for sample in $(ls -d P6161_10[56])
do
	cat $sample/*/*/*_R1_001.fastq.gz > $sample/R1.fastq.gz
	cat $sample/*/*/*_R2_001.fastq.gz > $sample/R2.fastq.gz
done

#./run_pipeline.sh


